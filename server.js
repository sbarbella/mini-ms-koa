const Koa = require('koa');
const app = new Koa();

const Router = require('koa-router');
const router = new Router();
const bodyParser = require('koa-parser');
const axios = require('axios');
//var logger = require('koa-pino-logger')
//const loggerPino = require('pino')();

const PORT = 3000;

const info = '{"name":"test","salary":"123","age":"23"}'; 
const GET = 'http://dummy.restapiexample.com/api/v1/employees'; 
const POST = 'http://dummy.restapiexample.com/api/v1/create';

// loggerPino.info('Outside');

router.get('/healthcheck', async (ctx) => {
   
    console.log(Math.round(Math.random()*2000)); 

    try{
        const response = await axios.get(GET);
        //logger.info(response.data);
        ctx.body = response.data; 
    } catch(e){
        throw(500, e);
    }

});

router.post('/healthcheck', async (ctx) =>{

    const body = ctx.request.body;
    console.log(Math.round(Math.random()*2000)); 
  
    try{
        const response = await axios.post(POST, info)
        ctx.body = response.data; 
        ctx.status = 200;
    
    }catch(e){
        throw(500, e);
    }
}); 

//app.use(logger())
app.listen(PORT); 
app.use(bodyParser());
app.use(router.routes());

console.log(`Server is listening on PORT ${PORT}`);




